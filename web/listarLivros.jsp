<%-- 
    Document   : listarAutores
    Created on : 13/03/2017, 20:27:15
    Author     : juliano.vieira
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!DOCTYPE html>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Listar livros</title>
</head>
<body>
    <p><a href="index.jsp">Voltar</a></p>
    <p><a href="Controller?action=AlterarLivro">Cadastrar novo livro</a></p>
    
    <div>        
        <form method="GET" action='Controller' name="formBuscarLivros">        
            Título do livro : <input type="text" name="nome" value="" />   
            <input type="hidden" value="ListarLivro" name="action" />
            <input type="submit" value="Buscar" />
        </form>        
    </div>
    
    <hr>
    
    <table border=0>
        <thead>
            <tr>
                <th>Codigo ISBN</th>
                <th>Título</th>
                
                <th colspan=2>Ação</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${livros}" var="livro">
                <tr>
                    <td><c:out value="${livro.isbn}" /></td>
                    <td><c:out value="${livro.titulo}" /></td>                    
                    <td><a href="Controller?action=AlterarLivro&codigo=<c:out value="${livro.isbn}"/>">Editar</a></td>
                    <td><a href="Controller?action=ExcluirLivro&codigo=<c:out value="${livro.isbn}"/>">Deletar</a></td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
    
</body>
</html>