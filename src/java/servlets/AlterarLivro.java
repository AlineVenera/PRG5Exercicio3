/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import Controller.Acao;
import bean.Livro;
import dao.DAOLivro;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author juliano.vieira
 */
public class AlterarLivro implements Acao {

    @Override
    public String executar(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DAOLivro daoLivro = new DAOLivro();        
        Livro livro = new Livro();
        livro.setDescricao(request.getParameter("descricao"));
        livro.setTitulo(request.getParameter("titulo"));
        //livro.setAutor(request.getParameter("codigoAutor"));
        livro.setEditora(request.getParameter("editora"));
        String codigo = request.getParameter("isbn");
        if(codigo == null || codigo.isEmpty()){
            daoLivro.addLivro(livro);
        }else{
            livro.setIsbn(codigo);            
            daoLivro.alterarLivro(livro);
        }
        request.setAttribute("livros", DAOLivro.getLivros());
        return "/listarLivros.jsp";
    }

}
