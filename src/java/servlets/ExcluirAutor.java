/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import Controller.Acao;
import bean.Autor;
import dao.DAOAutor;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author juliano.vieira
 */
public class ExcluirAutor implements Acao {

    @Override
    public String executar(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DAOAutor daoAutor = new DAOAutor();        
        int codigo = Integer.parseInt(request.getParameter("codigo"));
        daoAutor.deletarAutor(codigo);
        request.setAttribute("autores", daoAutor.getAutores());
        return "/listarAutores.jsp";
    }

}
