/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import bean.Autor;
import bean.Livro;
import static dao.DAOAutor.fabrica;
import fabricaDeSessoes.HibernateUtil;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 *
 * @author juliano.vieira
 */
public class DAOLivro {
 
    // DAO = Data Access Object
    // Usa os métodos de SessionFactory
        
    static SessionFactory fabrica = HibernateUtil.getSessionFactory();
    
    public void addLivro(Livro livro){               
        Session sessao = fabrica.openSession();
        sessao.beginTransaction();
        sessao.save(livro);
        sessao.getTransaction().commit();
        sessao.close();
    }
    
    public Livro getLivroPeloISBN(String isbn){
        Session sessao = fabrica.openSession();
        sessao.beginTransaction();
        Query q = sessao.createQuery("from Livro where isbn = :isbn");
        q.setParameter("isbn", isbn);
        sessao.close();
        return (Livro) q.uniqueResult();
    }
    
    public Livro getLivroPeloTitulo(String titulo){
        Session sessao = fabrica.openSession();
        sessao.beginTransaction();
        Query q = sessao.createQuery("from Livro where titulo = :titulo");
        q.setParameter("titulo", titulo);
        sessao.close();
        return (Livro) q.uniqueResult();
    }
    
    public static List getLivros() {
        Session sessao = fabrica.openSession();
        sessao.beginTransaction();
        Query q = sessao.createQuery("from Livro");
        List list = q.list();
        sessao.close();
        return list;
    }  
    
    public void alterarLivro(Livro livro){
        Session sessao = fabrica.openSession();
        sessao.beginTransaction();
        Query q = sessao.createQuery("update Livro set titulo = :titulo where isbn = :isbn");
        q.setParameter("titulo", livro.getTitulo());
        q.setParameter("isbn", livro.getIsbn());
        sessao.close();
    }
    
    public void deletarLivro(String isbn){
        Session sessao = fabrica.openSession();
        sessao.beginTransaction();
        Query q = sessao.createQuery("delete from Livro where isbn = :isbn");
        q.setParameter("isbn", isbn);
        sessao.close();
    }
    
    
        public Livro buscarLivro(String isbn){
        Session sessao = fabrica.openSession();
        sessao.beginTransaction();
        Query q = sessao.createQuery("from livro where nome = :isbn");
        q.setParameter("isbn", isbn);
        sessao.close();
        return (Livro) q.uniqueResult();
    }
    
    
}
